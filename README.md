Superstructure, a modular, highly customisable CSS framework being made with the developer in mind.

This framework offers a great deal of freedom to developers and desginers who jsut want to get on with the fun stuff: creating awesome visuals,
without having to worry about the groundwork that they build upon.

Almost every part of this framework can be customised with ease, and it is tested and optimised to work on a variety of device types and sizes.

CDN link:
http://protoplanetdigital-cdn.s3-ap-southeast-2.amazonaws.com/superstructure.min.css